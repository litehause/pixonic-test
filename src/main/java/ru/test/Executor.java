package ru.test;

import java.util.concurrent.*;

public class Executor extends Logger implements Runnable {

    private PriorityBlockingQueue<LocalDateTimeWithCallable> q = new PriorityBlockingQueue<>();

    private ExecutorService service;

    public Executor() {
        this.service = Executors.newFixedThreadPool(1);
    }

    public void run() {
            while (true) {
                try {
                    LocalDateTimeWithCallable value = q.peek();
                    if (value == null) {
                        TimeUnit.SECONDS.sleep(1000);
                        continue;
                    } else if (value.getLocaleDateTime() > System.currentTimeMillis()) {
                        Thread.sleep(value.getLocaleDateTime() - System.currentTimeMillis());
                        continue;
                    } else {
                        value = q.take();
                    }
                    Future<String> futureResult = service.submit(value.getValue());
                    String result = futureResult.get(10, TimeUnit.SECONDS);
                    getLogger().info(value.getLocaleDateTime() + " --- " + result);
                } catch (TimeoutException | ExecutionException  e) {
                    getLogger().error(e.getMessage(), e);
                } catch (InterruptedException ignored) {

                }
            }
    }

    public void add(LocalDateTimeWithCallable t, Thread thread) {
        this.q.add(t);
        thread.interrupt();
    }
}
