package ru.test;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;


public class LocalDateTimeWithCallable implements Comparable<LocalDateTimeWithCallable> {

    private static volatile AtomicLong incr = new AtomicLong();

    private final Long localeDateTime;
    private final String operationUUID;

    private final Callable<String> value;
    private final Long seqNumber;

    public LocalDateTimeWithCallable(Long localeDateTime, String value) {
        this.localeDateTime = localeDateTime;
        this.operationUUID = localeDateTime + "_" + incr.incrementAndGet();
        this.seqNumber = incr.incrementAndGet();
        this.value = new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(100);
                return value;
            }
        };
    }


    @Override
    public int compareTo(LocalDateTimeWithCallable o) {
        int res = this.localeDateTime.compareTo(o.localeDateTime);
        if (res == 0)
            res = (seqNumber < o.seqNumber ? -1 : 1);
        return res;
    }

    public String getOperationUUID() {
        return this.operationUUID;
    }

    public Long getLocaleDateTime() {
        return this.localeDateTime;
    }

    public Callable<String> getValue() {
        return this.value;
    }
}
