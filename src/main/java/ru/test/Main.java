package ru.test;


import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException, ParseException {
        InputStream is = Main.class.getClassLoader().getResourceAsStream("operations.txt");
        String inputStreamString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
        List<LocalDateTimeWithCallable> inputs = new ArrayList<>();
        Long stime = System.currentTimeMillis();
        for (String st : inputStreamString.split("\n")) {
            String[] dateNumber = st.split(" - ");
            Long time = stime + Long.parseLong(dateNumber[0]);
            String number = dateNumber[1];
            inputs.add(new LocalDateTimeWithCallable(time, number));
        }


        Executor executor = new Executor();
        Thread executorThread = new Thread(executor);
        executorThread.start();
        inputs.forEach((input) -> executor.add(input, executorThread));
    }

}
