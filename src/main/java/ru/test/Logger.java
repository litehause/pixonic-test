package ru.test;

import org.slf4j.LoggerFactory;

public abstract class Logger {
    private org.slf4j.Logger logger = LoggerFactory.getLogger("testLogger");

    public org.slf4j.Logger getLogger() {
        return this.logger;
    }
}
